//have fun
console.log("Would you like to play a game???");

//define any constants that I'm going to need
const input = require('readline-sync');

//define any functions that I'm going to need here
function numberCheck(userNumber, numberGenerated) {
  if (userNumber > numberGenerated) {
    return "Too high!";
  } else if (userNumber < numberGenerated) {
    return "Too low!";
  } else {
    return "Error!";
  }
}

//generate a random number
let numberGenerated = Math.floor(Math.random() * 10);
console.log(numberGenerated);

//get the user to input a number
let userNumber = Number(input.question("What number do you guess? "));
console.log(userNumber);

//call a function to see if the number is greater than, less than or equal to the generated number
//loop until the user guesses the number
while (userNumber != numberGenerated) {
  console.log(numberCheck(userNumber, numberGenerated));
  userNumber = input.question("Care to try again?");
}

console.log("You have guessed correctly!");
